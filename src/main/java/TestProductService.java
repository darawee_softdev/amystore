/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Darawee
 */
public class TestProductService {
    public static void main(String[] args) {
        System.out.println(ProductService.getProduct());
        //C
        ProductService.addProduct(new Product("xxx","xxx",10,1));
        System.out.println(ProductService.getProduct());
        //U
        Product updateProduct = new Product("yyy","yyy",1,1);
        ProductService.updateProduct(3, updateProduct);
        System.out.println(ProductService.getProduct());
        //D
        ProductService.delProduct(updateProduct);
        System.out.println(ProductService.getProduct());
        ProductService.delProduct(1);
        System.out.println(ProductService.getProduct());
    }
}
