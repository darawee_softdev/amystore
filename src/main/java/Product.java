
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Darawee
 * Product Model
 */
public class Product implements Serializable {

    private String productName;
    private String productBrand;
    private double price;
    private int amount;

    public Product(String productName, String productBrand, double price, int amount) {
        this.productName = productName;
        this.productBrand = productBrand;
        this.price = price;
        this.amount = amount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Product Name = " + productName + " Brand = " + productBrand
                + " Price = " + price + " Amount = " + amount;
    }
}
